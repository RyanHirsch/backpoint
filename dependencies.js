{
  'underscore-amd': 'https://raw.github.com/amdjs/underscore/master/underscore-min.js',
  'backbone-amd': 'https://raw.github.com/amdjs/backbone/master/backbone-min.js',
  'require': 'http://requirejs.org/docs/release/2.1.4/minified/require.js',
  'moment': 'https://raw.github.com/timrwood/moment/2.0.0/min/moment.min.js',
  'q': 'https://raw.github.com/kriskowal/q/master/q.min.js',
  'handlebars': 'https://raw.github.com/wycats/handlebars.js/1.0.0-rc.3/dist/handlebars.js',
  'text': 'https://raw.github.com/requirejs/text/latest/text.js',
  'jquery': 'http://code.jquery.com/jquery-1.8.3.min.js'
}