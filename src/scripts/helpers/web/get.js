define(['q', 'helpers/ctx/get'], function(Q, spContext) {
  "use strict";
  var getWeb = function(model, success, error) {
    var deferred = Q.defer();
    var web, context;

    var localSuccess = function (sender, args) {
      deferred.resolve(web);
      if(success) { success(web); }
    };

    var localError = function (sender, args) {
      deferred.reject(args.get_message());
      if(error) { error(args.get_message()); }
    };

    if(!model.get('context')) {
      spContext.get(model.get('serverRelativeUrl')).done(function(resolvedContext) {
        context = resolvedContext;
        web = context.get_web();
        context.load(web);
        context.executeQueryAsync(localSuccess, localError);
      });
    }
    else {
      context = model.get('context');
      web = context.get_web();
      context.load(web);
      context.executeQueryAsync(localSuccess, localError);
    }

    return deferred.promise;
  };

  return getWeb;
});