define(['q', 'helpers/ctx/get'], function(Q, spContext) {
  "use strict";
  var putWeb = function(model, success, error) {
    var deferred = Q.defer();
    var web, context;

    var localSuccess = function (sender, args) {
      deferred.resolve(web);
      if(success) success(web);
    };

    var localError = function (sender, args) {
      deferred.reject(args.get_message());
      if(error) error(args.get_message());
    };

    var updateWeb = function (web, model) {
      for(var objProperties in model.changedAttributes()) {
        web['set_' + objProperties](model.attributes[objProperties]);
      };
      web.update();
    };

    if(!model.get('context')) {
      spContext.get(model.get('serverRelativeUrl')).done(function(resolvedContext) {
        context = resolvedContext;
        web = context.get_web();
        updateWeb(web, model);
        context.load(web);
        context.executeQueryAsync(localSuccess, localError);
      });
    }
    else {
      context = model.get('context');
      web = context.get_web();
      updateWeb(web, model);
      context.load(web);
      context.executeQueryAsync(localSuccess, localError);
    }

    return deferred.promise;
  };

  return putWeb;
});