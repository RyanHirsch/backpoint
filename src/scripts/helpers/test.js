require(['q'],
function(Q) {
  window.gsp = function (func, obj, options) {
    var dfd = Q.defer(), result, context;

    var success = function(sender, args) {
      var results = obj[func]();
      dfd.resolve(results);
      if(options && options.success) { options.success(results) };
    };

    var error = function(sender, args) {
      dfd.reject(args);
      if(options && options.error) { options.error(args) };
    };

    try {
      dfd.resolve(obj[func]());
    }
    catch(e) {
      console.error(e);
      context = obj.get_context();
      context.load(obj);
      context.executeQueryAsync(success, error);
    }

    return dfd.promise;
  };
});

window.queryCount = 0;
gsp('get_lists', spweb).done(function(results {
  console.log(results);
  console.log("saved as query" + queryCount);
  window["query" + queryCount] = results;
  queryCount++;
});
