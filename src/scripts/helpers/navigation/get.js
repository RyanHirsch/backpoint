define(['q', 'helpers/ctx/get'], function(Q, spContext) {
  "use strict";
  var getNav = function(model, success, error) {
    var deferred = Q.defer();
    var context, web, nav, quickLaunch, topNavigationBar;

    var localSuccess = function (sender, args) {
      deferred.resolve(nav);
      if(success) { success(nav); }
    };

    var localError = function (sender, args) {
      deferred.reject(args.get_message());
      if(error) { error(args.get_message()); }
    };

    var _getNav = function(resolvedContext) {
      web = context.get_web();
      nav = web.get_navigation();
      quickLaunch = nav.get_quickLaunch();
      topNavigationBar = nav.get_topNavigationBar();

      context.load(nav);
      context.load(quickLaunch);
      context.load(topNavigationBar);
      context.executeQueryAsync(localSuccess, localError);
    };

    if(!model.get('context')) {
      spContext.get(model.contextUrl).done(function(resolvedContext) {
        context = resolvedContext;
        _getNav(context);
      });
    }
    else {
      context = model.get('context');
      _getNav(context);
    }

    return deferred.promise;
  };

  return getNav;
});