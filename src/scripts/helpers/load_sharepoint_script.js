define(['q'],
function(Q) {
  var _scriptPromises = {};

  var _load = function(scriptName, scriptObj) {
    var key = scriptName + "-" + scriptObj;
    if(_scriptPromises[key]) { return _scriptPromises[key]; }

    var deferred = Q.defer();

    var hasResolved = false;
    SP.SOD.executeFunc(scriptName, scriptObj, function () {
      hasResolved = true;
      deferred.resolve();
    });

    // This is a guard act, the above should be faster.
    // This shouldn't be needed but for some reason
    //   the sometimes the above never resolves
    SP.SOD.executeOrDelayUntilScriptLoaded(function () {
      if(!hasResolved) {
        // console.warn("executeFunc hasn't resolved as expected");
        hasResolved = true;
        deferred.resolve();
      }
    }, scriptName);

    _scriptPromises[key] = deferred.promise;
    return _scriptPromises[key];
  };

  return {
    context: _load('sp.js', 'SP.ClientContext'),
    notify: function() {
      return _load('sp.js', 'SP.UI.Notify');
    },
    script: function(scriptName, scriptObject) {
      return _load(scriptName, scriptObject);
    }
  };
});