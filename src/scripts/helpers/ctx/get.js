define(['q', 'helpers/load_sharepoint_script'], function(Q, LoadScript) {
  "use strict";
  var contextPromises = {};
  var _getContext = function (url) {
    var deferred;
    if(url) {
      if(contextPromises[url]) {
        return contextPromises[url];
      }
      else {
        deferred = Q.defer();
        LoadScript.context.done(function () {
          deferred.resolve(new SP.ClientContext(url));
        });
      }
    }
    else {
      if(contextPromises['current']){
        return contextPromises['current'];
      }

      deferred = Q.defer();
      LoadScript.context.done(function () {
        var current = new SP.ClientContext.get_current();
        contextPromises['current'] = deferred.promise;
        contextPromises[current.get_url()] = deferred.promise;
        deferred.resolve(current);
      });
    }
    contextPromises[url] = deferred.promise;
    return deferred.promise;
  };

  return {
    get: _getContext
  };
});