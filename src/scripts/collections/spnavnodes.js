define(['backbone', 'models/spnavigationnode'],
function(Backbone, NavNode) {
  "use strict";
  var NavNodes = Backbone.Collection.extend({
    model: NavNode,
    initalize: function(navNodes, options) {
      console.log("NavNodes Initalized");
      console.log(navNodes);
      console.log(options);
      if(!self.model) {
        NavNode = require('models/spnavigationnode');
        self.model = NavNode; // obviously we need re-set the model if it was
                            //    undefined due to circular dependencies
      }
    }
  });
  return NavNodes;
});