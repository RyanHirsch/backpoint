define(['backbone', 'models/spweb'],
function(Backbone, SPWeb) {
  "use strict";
  var WebsCollection = Backbone.Collection.extend({
    model: SPWeb,
    initialize: function(models , options) {
      if(options && options.webs instanceof SP.WebCollection) {
        this.get_webs(options.webs, options.ctx);
      }
    },
    //  http://sharepoint.stackexchange.com/questions/18776/fetch-list-of-sites-in-site-collection-into-jquery
    get_webs: function(webs, context) {
      var self = this;
      var success = function(sender, args) {
        var i, web;
        for (i = 0; i < webs.get_count(); i++) {
          web = webs.itemAt(i);
          // Guard against circular dependencies
          // http://stackoverflow.com/questions/14324535/circular-dependency-in-backbone-requirejs-nested-list
          // investigate https://github.com/PaulUithol/Backbone-relational
          //    to avoid the hack
          if(!self.model) {
            SPWeb = require('models/spweb');
            self.model = SPWeb; // obviously we need re-set the model if it was
                                //    undefined due to circular dependencies
          }
          var newWeb = new SPWeb({ url: web.get_serverRelativeUrl(), title: web.get_title() });
          self.add(newWeb);
        }
      };

      var error = function(sender, args) {
        console.log("Error");
        console.log(args.get_message());
      };

      context.load(webs);
      context.executeQueryAsync(success, error);
    }
  });

  return WebsCollection;
});
  