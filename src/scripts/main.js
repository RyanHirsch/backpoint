require(['backbone', 'models/spweb', 'collections/webs', 'models/spnavigation'],
function(Backbone, SPWeb, SPWebs, SPNav) {
  "use strict";
  window.Backpoint = {
    Models: {
      Web: SPWeb,
      Navigation: SPNav
    },
    Views: {
    },
    Collections: {
      Webs: SPWebs
    },
    Routers: {
    },
    Helpers: {
    }
  };

  window.currentweb = new window.Backpoint.Models.Web();
  window.currentweb.fetch({
    success: function(resolvedWeb) {
      console.log("fetch currentweb complete");
      console.log(currentweb.get('title'));
      console.log(currentweb.get('description'));
      // var quickLaunch = nav.get_quickLaunch();
      // => SP.NavigationNodeCollection
      
      // var topNavigationBar = nav.get_topNavigationBar();
      // => SP.NavigationNodeCollection
    }
  });
  window.navigation = new window.Backpoint.Models.Navigation();
});


window.success = function () {
  console.log("load complete");
};
window.error = function () {
  console.error("Error with fetch");
};