"use strict";
require.config({
  baseUrl: 'scripts',
  paths: {
    'jquery': 'vendor/jquery-1.8.3.min',
    'backbone': 'vendor/backbone-min',
    'underscore': 'vendor/underscore-min',
    'handlebars': 'vendor/handlebars',
    'q': 'vendor/q.min',
    'moment': 'vendor/moment.min',
    'text': 'vendor/text'
  },
  shim: {
    'handlebars': {
      exports: 'Handlebars'
    },
    'q': {
      exports: 'Q'
    }
  }
});