define(['backbone', 'collections/webs'],
function(Backbone, SPWebs) {
  "use strict";
  // Model to handle the SP.Web object
  var SPWeb = Backbone.Model.extend({
    defaults: {
      context: null,            // .get_context() =>
                                //          get_url() => "/personal/rhirsch"
                                //          get_site() => SP.Site - further info needs new request
                                //          get_web() => SP.Web

      id: null,                 // .get_id() => "6a4689d5-dafb-436e-b245-1d99dc4544f9"

      title: null,                // .get_title() => "Hirsch, Ryan"

      description: null,          // .get_description() => "This is the personal space of Hirsch, Ryan."

      serverRelativeUrl: null,  // .get_serverRelativeUrl() => "/personal/rhirsch"

      lists: null,              // .get_lists() => SP.ListCollection - futher info needs new request

      navigation: null,         // .get_navigation() => SP.Navigation
                                //          get_quickLaunch() => SP.NavigationNodeCollection

      webs: null                // .get_webs() => SP.WebCollection - further info needs new request
    },
    idAttribute: "serverRelativeUrl",
    initialize: function(attributes, options) {
      if(attributes && attributes.url) { this.set('serverRelativeUrl', attributes.url); }
    },
    parse: function(spweb) {
      //Circular dependency hack
      if(!SPWebs) { SPWebs = require('collections/webs'); }
      window.spweb = spweb;
      var respObj = {
        context:            spweb.get_context(),
        id:                 spweb.get_id().toString(),
        title:              spweb.get_title(),
        description:        spweb.get_description(),
        serverRelativeUrl:  spweb.get_serverRelativeUrl(),
        webs:               new SPWebs( null, {
                              webs:   spweb.get_webs(),
                              ctx:    spweb.get_context()
                            }),
        navigation:         spweb.get_navigation()
        //lists:              new Backpoint.Collections(spweb.get_lists()),
        //navigation:         new Backpoint.Collections.Navigation(spweb
        //                        .get_navigation()),
      };
      return respObj;
    },
    sync: function(method, model, options) {
      // console.log("method: " + method);console.log(model);console.log(options);
      var self = this;

      var success = options.success;
      options.success = function(resp) {
        // to handle undocumented 0.9.10 changes to the callback
        // see - https://github.com/documentcloud/backbone/pull/2281

        // the line below invokes our parser, as does the subsequent line,
        //    seems undesirable, so we will bypass it for now.
        //if (!model.set(model.parse(resp, options), options)) { return false; }
        if (success) { success(model, resp, options); }
      };

      switch(method) {
        case "read":
          require(['helpers/web/get'], function(getWeb) {
            getWeb(model)
              .then(options.success)
              .fail(function(errorObj) {
                // console.error(errorObj);
                // window.err = errorObj;
              });
          });
        break;
        case "update":
          require(['helpers/web/put'], function(putWeb) {
            putWeb(model).done(options.success);
          });
        break;
      }
    }
  });
  return SPWeb;
});
// TODO: Get parent SPWeb
// TODO: Get Lists
// TODO: Get Navigation


// site main.js:26
// 97e8aa0f-70ac-4235-885c-05b861f36f58 main.js:27
// http://my.cti-w.com/personal/rhirsch main.js:28
// web main.js:30
// 6a4689d5-dafb-436e-b245-1d99dc4544f9 main.js:31
// /personal/rhirsch


// site main.js:26
// 97e8aa0f-70ac-4235-885c-05b861f36f58 main.js:27
// http://my.cti-w.com/personal/rhirsch main.js:28
// web main.js:30
// 140da931-6d14-4da1-ae5e-3c76802e18e5 main.js:31
// /personal/rhirsch/Blog

    // success: function(resolvedWeb) {
    //   var ctx = window.currentweb.get('context');
    //   var site = ctx.get_site();
    //   ctx.load(site);
    //   ctx.executeQueryAsync(function() {
    //     console.log('site');
    //     console.log(site.get_id().toString());
    //     console.log(site.get_url());
    //     console.log('web');
    //     console.log(currentweb.get('id'));
    //     console.log(currentweb.get('serverRelativeUrl'));
    //   });
    // }