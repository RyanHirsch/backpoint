define(['backbone', 'collections/spnavnodes'],
function(Backbone, NavNodes) {
  "use strict";
  // Model to handle the SP.Navigation object
  var SPNavigation = Backbone.Model.extend({
    defaults: {
      context:            null,
      useShared:          null,   // boolean
      quickLaunch:        null,   // SP.NavigationNodeCollection => Backpoint.Collections.NavigationItems
      topNavigationBar:   null    // SP.NavigationNodeCollection => Backpoint.Collections.NavigationItems
    },
    initialize: function(attributes, options) {
      if(attributes && attributes.url) { this.contextUrl = attributes.url; }
      this.fetch({
        success: function(spnav) {
          console.log("NAVIGATION WORKS?");
        }
      });
    },
    sync: function(method, model, options) {
      console.log("method: " + method);console.log(model);console.log(options);
      var self = this;

      var success = options.success;
      options.success = function(resp) {
        // to handle undocumented 0.9.10 changes to the callback
        // see - https://github.com/documentcloud/backbone/pull/2281

        // the line below invokes our parser, as does the subsequent line,
        //    seems undesirable, so we will bypass it for now.
        //if (!model.set(model.parse(resp, options), options)) { return false; }
        if (success) { success(model, resp, options); }
      };

      switch(method) {
        case "read":
          require(['helpers/navigation/get'], function(getNav) {
            getNav(model)
              .then(options.success)
              .fail(function(errorObj) {
                console.error(errorObj);
                window.err = errorObj;
              });
          });
        break;
        case "update":
          require(['helpers/nav/put'], function(putWeb) {
            putWeb(model).done(options.success);
          });
        break;
      }
    },
    parse: function(spnavigation) {
      var quickLaunchArray = spnavigation.get_quickLaunch().get_data();
      var topNavigationBarArray = spnavigation.get_topNavigationBar().get_data();

      var rtnObj = {
        context:            spnavigation.get_context(),
        useShared:          spnavigation.get_useShared(),
        quickLaunch:        new NavNodes(quickLaunchArray, {parse: true}),
        topNavigationBar:   new NavNodes(topNavigationBarArray, {parse: true})
      };
      return rtnObj;
    }
  });

  return SPNavigation;
});


// http://msdn.microsoft.com/en-us/library/ee537538%28v=office.14%29.aspx
// nav = .get_navigation()
//      => SP.Navigation
//          => .get_topNavigationBar() => SP.NavigationNodeCollection
//          => .get_quickLaunch() => SP.NavigationNodeCollection
//          => .get_useShared() => boolean
//
//  => SP.NavigationNodeCollection
//      .get_data()
//          => [SP.NavigationNode , ... ]
//            .get_title()
//            .get_url()
//            .get_children() => SP.NavigationNodeCollection