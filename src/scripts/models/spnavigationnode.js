define(['backbone', 'collections/spnavnodes'],
function(Backbone, SPNavNodes) {
  "use strict";
  // SP.NavigationNode
  var SPNavigationNode = Backbone.Model.extend({
    defaults: {
      title:    null,   // string
      url:      null,   // string
      id:       null,   // int
      children: null    // => SP.NavigationNodeCollection   -> SPNavigationNodes
    },
    parse: function(navNode) {
      var childrenArray = navNode.get_children().get_data();
      if(!SPNavNodes) { SPNavNodes = require('collections/spnavnodes'); }

      console.log("Parsing NavNode");
      console.log(childrenArray);
      console.log(navNode.get_title());
      window.children = navNode.get_children();

      var rtnObject = {
        title:      navNode.get_title(),
        url:        navNode.get_url(),
        id:         navNode.get_id(),
        children:   new SPNavNodes(childrenArray, {parse: true} )
      };
      return rtnObject;
    },
    initialize: function(attributes, options) {
      console.log("initializing SP.NavigationNode");
    }
  });
  return SPNavigationNode;
});


// define(['backbone', 'collections/spnavnodes'],
// function(Backbone, SPNavNodes) {
//   "use strict";
//   // SP.NavigationNode
//   var SPNavigationNode = Backbone.Model.extend({
//     defaults: {
//       title:    null,   // string
//       url:      null,   // string
//       id:       null,   // int
//       children: null    // => SP.NavigationNodeCollection   -> SPNavigationNodes
//     },
//     parse: function(navNode) {
//       if(!SPNavNodes) { SPNavNodes = require('collections/spnavnodes'); }
      
//       var childrenSpCollection = navNode.get_children();

//       var rtnObject = {
//         title:      navNode.get_title(),
//         url:        navNode.get_url(),
//         id:         navNode.get_id()
//       };
//       return rtnObject;
//     },
//     initialize: function(attributes, options) {
//       if(options && options)
//     }
//   });
//   return SPNavigationNode;
// });