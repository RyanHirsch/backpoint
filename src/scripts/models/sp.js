define(['backbone'],
function(Backbone) {
  "use strict";
  // Model to handle the SP.Web object
  var SP = Backbone.Model.extend({
    defaults: {
      url: null
    },
    initialize: function(attributes, options) {
    }
  });
  return SP;
});


/*

ctx
  => .get_site
  => .get_webs        => SP.Web
    => .get_created             => string
    => .get_description         => string
    => .get_title               => string
    => .get_serverRelativeUrl   => string
    => .get_quickLaunchEnabled  => boolean
    => .get_id                  => SP.Guid
    => .get_currentUser         => SP.User
      => .get_email               => string
      => .get_id                  => int
      => .get_loginName           => string
      => .get_title               => string
    => .get_navigation          => SP.Navigation
      => .get_quickLaunch         => SP.NavigationNodeCollection
        => SP.NavigationNode
          => .get_id                  => int
          => .get_title               => string
          => .get_url                 => string
          => .get_children            => SP.NavigationNodeCollection
      => .get_topNavigationBar    => SP.NavigationNodeCollection
        => SP.NavigationNode
          => .get_id                  => int
          => .get_title               => string
          => .get_url                 => string
          => .get_children            => SP.NavigationNodeCollection
    => .get_lists               => SP.ListCollection
      => .get_count               => int
      => .get_data               => [ SP.List ]
        => SP.List
          => .get_id                 => SP.Guid
          => .get_itemCount          => int
          => .get_title              => string
          => .get_description        => string
          => .get_views              => SP.ViewCollection
            => .get_                   =>                   
          => .get_fields             => SP.FieldCollection
    => .get_webs                => SP.WebCollection
*/